export const state = () => ({
  cities: [],
})
export const mutations = {
  ADD_CITY(state, cityData) {
    state.cities.push(cityData)
  },
  DELETE_CITY(state, id) {
    state.cities.splice(id, 1)
  },
  UPDATE_FORECAST(state, { index, html }) {
    state.cities[index].html = html
  },
}
