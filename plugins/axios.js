import Vue from 'vue'

export default function ({ $axios }) {
  $axios.onRequest((config) => {
    if (
      typeof config.url !== 'undefined' &&
      typeof config.data !== 'undefined'
    ) {
      process.env.DEBUG === 'on' &&
        console.log(
          'Making request to ' + config.url,
          'Send data ',
          config.data
        )
    }
  })

  $axios.onResponse((response) => {})
}
